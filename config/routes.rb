Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/accounts/index', to: 'accounts#index', as: 'accounts'
  get '/accounts', to: 'accounts#index', as: 'accounts_list'
  get '/accounts/new', to: 'accounts#new', as: 'new_account'
  post '/accounts/create', to: 'accounts#create', as: 'account_create'
  get '/accounts/:id', to: 'accounts#show', as: 'account'
  get '/accounts/:id/new_deposit', to: 'accounts#new_deposit', as: 'new_deposit'
  get '/accounts/:id/new_withdraw', to: 'accounts#new_withdraw', as: 'new_withdraw'
  post '/accounts/:id/deposit', to: 'accounts#deposit', as: 'deposit'
  post '/accounts/:id/withdraw', to: 'accounts#withdraw', as: 'withdraw'

  root to: 'accounts#index'
end
