# adds reference to transactions table
class AddAccountTransacrionReference < ActiveRecord::Migration[6.1]
  def change
    add_reference :transactions, :account, index: true, foreign_key: { to_table: :accounts }
  end
end
