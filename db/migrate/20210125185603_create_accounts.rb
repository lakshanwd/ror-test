# create table definition for accounts
class CreateAccounts < ActiveRecord::Migration[6.1]
  def change
    create_table :accounts do |t|
      t.string :no

      t.timestamps
    end
  end
end
