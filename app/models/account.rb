# model account
class Account < ApplicationRecord
  has_many :transactions

  def deposit(amount)
    ActiveRecord::Base.transaction do
      transaction = Transaction.create(account_id: id, amount: amount)
      self.balance = [] if balance.nil?
      self.balance += transaction.amount
      save!
    end
  end

  def withdraw(amount)
    ActiveRecord::Base.transaction do
      transaction = Transaction.create(account_id: id, amount: -amount.to_i)
      self.balance = [] if self.balance.nil?
      self.balance += transaction.amount
      save!
    end
  end
end
