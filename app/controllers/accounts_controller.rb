# accounts controller
class AccountsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @accounts = Account.where(['user_id = ?', current_user.id])
  end

  def show
    @account = Account.find_by(id: params[:id])
  end

  def create
    Account.create(no: params[:no], balance: 0, user_id: current_user.id)
    redirect_to action: 'index'
  end

  def deposit
    account = Account.find(params[:id])
    account.deposit(params[:amount])
    redirect_to account_url(account)
  end

  def withdraw
    account = Account.find(params[:id])
    account.withdraw(params[:amount])
    redirect_to account_url(account)
  end

  def new
    render :new
  end

  def new_withdraw
    @account = Account.find(params[:id])
  end

  def new_deposit
    @account = Account.find(params[:id])
  end
end
